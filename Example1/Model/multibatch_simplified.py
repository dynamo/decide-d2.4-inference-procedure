import os 
from   pathlib                     import Path
import csv
import dateutil.parser             as     dup
import numpy                       as     np
#import pandas                      as pd
from   emulsion.agent.managers     import MetapopProcessManager
from emulsion.agent.managers import MultiProcessManager
from   emulsion.tools.preprocessor import EmulsionPreprocessor
from   emulsion.tools.debug        import debuginfo
import random
from itertools import chain


class Multibatch(MetapopProcessManager):

    """Level of the population.

    """
    def initialize_level(self, **others):
        """Initialize an instance of Multibatch. Especially, after all regular
        initial conditions have been applied, pick random animals
        according to initial prevalence to infect them.

        """
        nb_lowrisk = int(np.round(self.get_model_value('prop_lowrisk') * self.get_model_value('batch_size')* self.get_model_value('number_batches')))
        nb_mediumrisk = int(np.round(self.get_model_value('prop_mediumrisk') * self.get_model_value('batch_size')* self.get_model_value('number_batches')))
        nb_highrisk = int(np.round(self.get_model_value('prop_highrisk') * self.get_model_value('batch_size')* self.get_model_value('number_batches')))

        temp_batch=self.new_atom()
        #temp_batch.new_atom(prototype='default_beef_bull')
        list_lowrisk = [temp_batch.new_atom(prototype='default_beef_bull',execute_actions=True) for i in range(nb_lowrisk)]
        for animal in list_lowrisk:
            animal.apply_prototype("initial_lowrisk",execute_actions=True)
        list_mediumrisk = [temp_batch.new_atom(prototype='default_beef_bull',execute_actions=True) for i in range(nb_mediumrisk)]
        for animal in list_mediumrisk:
            animal.apply_prototype("initial_mediumrisk",execute_actions=True)
        list_highrisk = [temp_batch.new_atom(prototype='default_beef_bull',execute_actions=True) for i in range(nb_highrisk)]
        for animal in list_highrisk:
            animal.apply_prototype("initial_highrisk",execute_actions=True)
        concat_list=list_lowrisk+list_mediumrisk+list_highrisk
        batches = self.get_populations()
        #print(len(batches))
        if self.get_model_value('sorted_batches')==1:
            for i in range(len(batches)):
                set_atom=list(concat_list[int(i*self.get_model_value('batch_size')):int((i+1)*self.get_model_value('batch_size'))])
                risk = str(set_atom[0].get_information('risk_status')).split('.')[1]
                batches[i].add_atoms(atom_set=set_atom)
                batches[i].statevars.lowrisk_batch=int(risk=='LowRisk')
                batches[i].statevars.mediumrisk_batch=int(risk=='MediumRisk')
                batches[i].statevars.highrisk_batch=int(risk=='HighRisk')
        else:
            np.random.shuffle(concat_list)
            for i in range(len(batches)):
                set_atom=set(concat_list[int(i*self.get_model_value('batch_size')):int((i+1)*self.get_model_value('batch_size'))])
                
                batches[i].add_atoms(atom_set=set_atom)

        







#===============================================================
# CLASS Pen (LEVEL 'pen')
#===============================================================
class Batch(MultiProcessManager):

    """Level of the pen.

    """

    #----------------------------------------------------------------
    # Level initialization
    #----------------------------------------------------------------
        

