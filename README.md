# DECIDE D2.4 Inference procedure

## Notes

- Before the execution of this code, you must first have EMULSION installed on your computer.  We invite you to go to this link for the installation procedure "https://sourcesup.renater.fr/www/emulsion-public/devel/"

- The names of scripts or templates may be the same for different examples, but this does not mean that they have the same content.


## Content of the repository
----------------------------------
  | File name        | Role                                        |
  |------------------|---------------------------------------------|
  | `README.md`      | This file                                   |
  | `Example1`          | Containing all the necessary information to reproduce the example 1 |
  | `Example2`          |Containing all the necessary information to reproduce the example 2|
  | `Example3`           | Containing all the necessary information to reproduce the example 3|
  | `Example4`           | Containing all the necessary information to reproduce the example 4 |

  Each of examples containing three folders :  'Model', 'Scripts' and 'Data'

  | File name        | Role                                        |
  |------------------|---------------------------------------------|
  | `README.md`      | This file                                   |
  | `Model`          | The folder contains the BRD model and dependent files      |
  | `Scripts`        | The folder contains useful scripts |
  | `Data`           | The folder contains useful data |

### Data

| Examples        | Data        | Role                                                 |
|------------------------|------------------|------------------------------------------------------|
| Exemple 1       |                        |                                           |
|                        | `EX1_ParamStatSim.RData` | RDATA file contains parameter sets and simulated summary statistics used for Example 1 when using the "abc" package  |
|                        | `EX1_ssobs.csv`           | CSV file contains synthetic data used as summary statistics observed  for Example 1|
|                        | `EX1_Median.csv` | CSV file contains the the median trajectory |
| Exemple 2       |                        |                                           |
|                        | `EX2_ParamStatSim.RData` | RDATA file contains parameter sets and simulated summary statistics used for Example 2 when using the "abc" package  |
|                        | `EX2_ssobs.csv`           | CSV file contains synthetic data used as summary statistics observed for Example 2|
| Exemple 3       |                        |                                           |
|                        | `EX3_ssobs.csv`           | CSV file contains synthetic data used as summary statistics observed  for Example 3|
| Exemple 4       |                        |                                           |
|                        | `EX4_ssobs_MED.csv`           | CSV file contains the median synthetic data used as summary statistics observed  for Example 4|
|                        | `EX4_ssobs_Q1.csv`           | CSV file contains the Q1 synthetic data used as summary statistics observed  for Example 4|
|                        | `EX4_ssobs_Q3.csv`           | CSV file contains the Q3 synthetic data used as summary statistics observed  for Example 4|


### Scripts

| Examples        | Scripts        | Role                                                 |
|--------------------|------------------|------------------------------------------------------|
| Exemple 1       |                        |                                           |
|                        | `runABCpackage.R` | R script used to run the "abc" package  for Example 1 |
|                        | `runBRREWABCpackage.R`           | R script used to run the "BRREWABC" package  for Example 1|
| Exemple 2       |                        |                                           |
|                        | `runABCpackage.R` | R script used to run the "abc" package  for Example 2 |
|                        | `runBRREWABCpackage.R`           | R script used to run the "BRREWABC" package  for Example 2|
| Exemple 3       |                        |                                           |
|                        | `runBRREWABCpackage.R`           | R script used to run the "BRREWABC" package  for Example 3|
| Exemple 4       |                        |                                           |
|                        | `runBRREWABCpackage.R`           | R script used to run the "BRREWABC" package  for Example 4|


### Model

| Examples        | Model        | Role                                                 |
|--------------------|------------------|------------------------------------------------------|
| Exemple 1       |                        |                                           |
|                        | `BRD_model.yaml` | YAML file describing the BRD model used in Section 2 of deliverable  |
|                        | `multibatch_simplified.py`           | PYTHON file ("BRD_model.yaml") contains the code needed to run the BRD model  |
|                        | `run_emulssion.R`           | R script to run the BRD model  |
| Exemple 2       |                        |                                           |
|                        | `BRD_model.yaml` | YAML file describing the BRD model used in Section 2 of deliverable  |
|                        | `multibatch_simplified.py`           | PYTHON file ("BRD_model.yaml") contains the code needed to run the BRD model  |
|                        | `run_emulssion.R`           | R script to run the BRD model  |
| Exemple 3       |                        |                                           |
|                        | `BRD_model.yaml` | YAML file describing the BRD model used in Section 2 of deliverable  |
|                        | `multibatch_simplified.py`           | PYTHON file ("BRD_model.yaml") contains the code needed to run the BRD model  |
|                        | `run_emulssion.R`           | R script to run the BRD model  |
| Exemple 4       |                        |                                           |
|                        | `BRD_model.yaml` | YAML file describing the BRD model used in Section 2 of deliverable  |
|                        | `multibatch_simplified.py`           | PYTHON file ("BRD_model.yaml") contains the code needed to run the BRD model  |
|                        | `run_emulssion.R`           | R script to run the BRD model  |